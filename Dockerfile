FROM trafex/php-nginx:latest

# Install dependencies
USER root

RUN apk add --no-cache \
  php81-pdo_mysql \
  php81-tokenizer \
  php81-dom \
  php81-fileinfo \
  php81-xmlwriter \
  && rm /var/www/html/*

USER nobody

# Install composer from the official image
COPY --from=composer /usr/bin/composer /usr/bin/composer

# Copy sources
COPY . /var/www/html