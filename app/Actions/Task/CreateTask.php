<?php

namespace App\Actions\Task;

use App\Enums\TaskStatus;
use App\Http\Requests\Api\Task\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Support\Carbon;

class CreateTask {
  public function __construct(
    // 🍰 Always use strong types
    public readonly string $name,
    public readonly int $boardId,
    public readonly int $creatorId,
    public readonly TaskStatus $status,
    public readonly ?string $description,
    public readonly ?Carbon $deadline,
  ) {}

  public function execute(): Task {
    return Task::create([
        'board_id' => $this->boardId,
        'name' => $this->name,
        'description' => $this->description,
        'deadline' => $this->deadline,
        'creator_id' => $this->creatorId,
        'status' => $this->status,
    ]);
  }

  // public static function fromRequest(CreateTaskRequest $request): self {
  //   return self(
  //     name: $request->name,
  //     boardId: $request->boardId,
  //     creatorID: $request->user()->id,
  //     description: $request->description,
  //     deadline: $request->deadline ? Carbon::parse($request->deadline) : null,
  //     status: TaskStatus::TODO
  //   );
  // }
}