<?php

namespace App\Actions\Task;

use App\Enums\TaskStatus;
use App\Http\Requests\Api\Task\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Support\Carbon;

class UpdateTask {
  protected array $newAttributes = [];

  public function __construct(
    public readonly Task $task,  
  ) {}

  public function setNewName(string $value) {
    $this->newAttributes['name'] = $value;
  }
  
  // 🍰 
  // public function setNewStatus(TaskStatus $value) {
  //   $this->newAttributes['status'] = $value;
  // }

  public function setNewDescription(?string $value) {
    $this->newAttributes['description'] = $value;
  }

  public function setNewDeadline(?Carbon $value) {
    $this->newAttributes['deadline'] = $value;
  }

  public function execute(): Task
  {
    $this->task->update([
      'name' => $this->getFinal('name'),
      'description' => $this->getFinal('description'),
      'deadline' => $this->getFinal('deadline'),
      // 'status' => $this->getFinal('status'),
    ]);

    return $this->task;
  }

  private function getFinal(string $attribute) {
    return $this->newAttributes[$attribute] ?? $this->task->$attribute;
  }
}