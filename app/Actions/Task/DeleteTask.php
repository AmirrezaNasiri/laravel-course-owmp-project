<?php

namespace App\Actions\Task;

use App\Enums\TaskStatus;
use App\Http\Requests\Api\Task\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Support\Carbon;

class DeleteTask {

  public function __construct(
    public readonly Task $task,  
  ) {}

  public function execute(): bool
  {
    return $this->task->delete();
  }
}