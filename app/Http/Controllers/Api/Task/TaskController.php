<?php

namespace App\Http\Controllers\Api\Task;

use App\Actions\Task\CreateTask;
use App\Actions\Task\DeleteTask;
use App\Actions\Task\UpdateTask;
use App\Enums\TaskStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Task\CreateTaskRequest;
use App\Http\Requests\Api\Task\UpdateTaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TaskController extends Controller
{
    // 🍰 Watch EmergencyTaskController

    public function get(Task $task)
    {
        return TaskResource::make($task);
    }

    public function index(Request $request)
    {
        $tasks = $request->user()->tasks()->paginate();
        return TaskResource::collection($tasks);
    }

    public function create(CreateTaskRequest $request)
    {
        $action = new CreateTask(
            name: $request->name,
            boardId: $request->board_id,
            creatorId: $request->user()->id,
            description: $request->description,
            deadline: $request->deadline ? Carbon::parse($request->deadline) : null,
            status: TaskStatus::TODO
        );

        return $action->execute();

        // 🍰
        // return TaskResource::make(
        //     $request->makeAction()->execute()
        // );

        // 🍰
        // return TaskResource::make(
        //     CreateTask::fromRequest($request)->execute()
        // );
    }

    public function update(Task $task, UpdateTaskRequest $request)
    {
        $action = new UpdateTask($task);

        if ($request->has('name')) {
            $action->setNewName($request->name);
        }

        if ($request->has('description')) {
            $action->setNewDescription($request->description);
        }

        if ($request->has('deadline')) {
            $action->setNewDeadline(Carbon::parse($request->deadline));
        }

        return TaskResource::make(
            $action->execute()
        );
    }

    public function destroy(Task $task)
    {
        return (new DeleteTask($task))->execute();
    }
}
