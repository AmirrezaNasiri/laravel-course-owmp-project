<?php

namespace App\Http\Requests\Api\Task;

use App\Actions\Task\CreateTask;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use App\Enums\TaskStatus;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'string',
                'min:4',
                'max:100',
            ],
            'description' => [
                'nullable',
                'string',
                'max:5000',
            ],
            'deadline' => [
                'nullable',
                'string',
                'date',
            ],
        ];
    }
}
