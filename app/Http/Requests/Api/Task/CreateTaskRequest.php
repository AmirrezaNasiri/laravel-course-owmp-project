<?php

namespace App\Http\Requests\Api\Task;

use App\Actions\Task\CreateTask;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use App\Enums\TaskStatus;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'min:4',
                'max:100',
            ],
            'description' => [
                'string',
                'max:5000',
            ],
            'deadline' => [
                'string',
                'date',
            ],
            'board_id' => [
                'required',
                Rule::exists('boards', 'id')->where(function (Builder $query) {
                    return $query->where('creator_id', request()->user()->id);
                }),
            ]
        ];
    }

    // public function makeAction(): CreateTask {
    //     return new CreateTask(
    //         name: $this->name,
    //         boardId: $this->board_id,
    //         creatorId: $this->user()->id,
    //         description: $this->description,
    //         deadline: $this->deadline ? Carbon::parse($this->deadline) : null,
    //         status: TaskStatus::TODO
    //     );
    // }
}
